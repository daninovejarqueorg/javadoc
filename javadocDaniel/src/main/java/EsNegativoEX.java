/**
 *Indica si el valor es negativo
 * @author Daniel
 * @version 1.2 26/04/2021
 */
public class EsNegativoEX extends Exception {
public EsNegativoEX() {
    super("el valor no puede ser negativo");
}
/**
 * 
 * @param msg Lo que hace super
 */
public EsNegativoEX(String msg) {
super(msg);
}
}
