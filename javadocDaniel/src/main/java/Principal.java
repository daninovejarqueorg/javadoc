/**
 *Devuelve si diferentes numeros introducidos son capicua(True) o si no lo son (False). 
 * @author Daniel
 * @version 1.2 26/04/2021
 */
public class Principal {
public static void main(String[] args) {
try {
System.out.println("12345 es capicúa: " + SoyUtil.esCapikua(12345));
System.out.println("1221 es capicúa: " + SoyUtil.esCapicua(1221));
System.out.println("1234321 es capicúa: " + SoyUtil.esCapikua(1234321));
} catch (EsNegativoEX ex) {
}
}
}