/**
 *
 * @author Daniel
 * @version 1.2 26/04/2021
 */
public class SoyUtil {
/**
 * 
 * @param numero
 * @return
 * @throws EsNegativoEX 
 */
public static boolean esCapicua(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
int numAlReves = 0;
int copia = numero;
while (numero > 0) {
numAlReves = numAlReves * 10 + numero % 10;
numero /= 10;
}
return copia == numAlReves;
}
/**
 * Clase que devuelve TRUE o FALSE en funcion de si el numero en cuestion es capicua
 * @param numero Valor numérico que se comprueba si es negativo y en caso de ser positivo se comprueba si es capicua.
 * @return Devuelve un TRUE o FALSE en funcion de si cadNum y numAlReves son iguales o no
 * @throws EsNegativoEX Excepción en caso de ser negativo el parametro "numero"
 */
public static boolean esCapikua(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
String cadNum = numero + "";
String numAlReves = (new StringBuilder(cadNum)).reverse().toString();
return cadNum.equals(numAlReves);
}
/**
 * Clase que devuelve TRUE o FALSE en funcion de si el numero en cuestion es primo o no.
 * @param numero Valor numérico sobre el cual se comprueba si es primo o no o si es negativo o no.
 * @throws EsNegativoEX Excepción en caso de ser negativo el parametro "numero"
 * return Devuelve TRUE o FALSE según sea primo o no.
 */
public static boolean esPrimo(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
int limite = numero / 2 + 1;
int div = 2;
while (div < limite) {
if (numero % div == 0) {
return false;
}
div++;
}
return true;
}
/**
 * Devuelve el factorial del numero en cuestion
 * @param numero Valor numérico sobre el cual se van a realizar las operaciones pertinentes para sacar el factorial.
 * @return Devuelve el factorial en caso de no haber introducido un numero negativo.
 * @throws EsNegativoEX Excepción en caso de ser negativo el parametro "numero"
 */
public static long getFactorial(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX(
"no se puede calcular el factorial de un número negativo");
}
long fact = 1L;
while (numero > 1) {
    fact *= numero;
numero--;
}
return fact;
}
}
